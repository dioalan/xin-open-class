import { define } from '@xinix/xin';
import { View } from '@xinix/xin/components';

import html from './dio-walkthrough.html';
import './dio-walkthrough.scss';

import('xin-ui/ui-slides');

export class MyWalkthrough extends View {
  get template () {
    return html;
  }
}
define('dio-walkthrough', MyWalkthrough);
