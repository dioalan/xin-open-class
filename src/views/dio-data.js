import { define } from '@xinix/xin';
import { View } from '@xinix/xin/components';

import $ from 'jquery';

import html from './dio-data.html';
import('xin-ui/ui-reveal');
import('xin-ui/ui-textfield');
import('xin-ui/ui-slides');


export class MyData extends View {
  static get is () {
    return 'dio-data';
  }

  get props () {
    return Object.assign({}, super.props, {
	  	data: {
	        type: Array,
	        value: [],
	    },
	    text: {
	    	type: String,
	        value: '',
	    },
    });
  }
  async focused (){
    super.focused();
    let response = await $.get('http://localhost/back-end-xin/www/index.php/user.json');
    this.set('data', response.entries);
    // console.log(response);

  }

  get template () {
    return html;
  }

    async doFilter(evt) {
    evt.preventDefault()
    // console.log(this.text);
    let response = await $.get('http://localhost/back-end-xin/www/index.php/user.json?!search='+ this.text);
    this.set('data', response.entries);
    // console.log(this.data);



  }
}
define('dio-data', MyData);
