import { define } from '@xinix/xin';
import { View } from '@xinix/xin/components';

import html from './dio-home.html';
import './dio-home.scss';

import('xin-ui/ui-slides');

export class DioHome extends View {
  get props () {
    return Object.assign({}, super.props, {
      title: {
        type: String,
        value: 'Home',
      },
      arr: {
        type: Array,
        value: [1, 2, 3, 4],
      },
      // obj: {
      //   type: Object,
      //   value: {1: a, 2: b, c: 3, d: 4},
      // },
      num: {
        type: Number,
        value: 0,
      },
      bol: {
        type: String,
        value: 0,
      },

    });
  }

  get template () {
    return html;
  }
  focused(){
    // console.log(this.obj);
    console.log(this.arr);
    console.log(this.num);
    console.log(this.bol);

  }
  doLogin (evt) {
    evt.preventDefault();

    this.__app.navigate('/');
  }
}
define('dio-home', DioHome);
