import { define } from '@xinix/xin';
import { View } from '@xinix/xin/components';

import html from './dio-menu.html';
import './dio-menu.scss';

export class MyMenu extends View {
  static get is () {
    return 'dio-menu';
  }

  get props () {
    return Object.assign({}, super.props, {
      title: {
        type: String,
        value: 'Menu',
      },
      menus: {
        type: Array,
        value: () => ([]),
      },
    });
  }

  get template () {
    return html;
  }
}
define('dio-menu', MyMenu);
