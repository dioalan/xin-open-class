import { define } from '@xinix/xin';
import { View } from '@xinix/xin/components';
import $ from 'jquery';

import html from './dio-login.html';

import('xin-ui/ui-reveal');
import('xin-ui/ui-textfield');

export class MyLogin extends View {
  static get is () {
    return 'dio-login';
  }

  get props () {
    return Object.assign({}, super.props, {
      title: {
        type: String,
        value: 'Login',
      },
      username: {
        type: String,
        value: '',
      },
      password: {
        type: String,
        value: '',
      },
      obj: {
        type: Object,
        value: '',
      },
      // arr: {
      //   type: Array,
      //   value: ['aaa','bbb','ccc'],
      // },
      arr: {
        type: Array,
        value: [{alamat:'jkt'},{nama:'dio', alamat:'bks'},{nama:'arik'}],
      },
      data: {
        type: Array,
        value: [],
      },
    });
  }

  _computingNama(item){
    if (!item) {
      return '';
    }
    if(!item.nama){
      return 'tdk ada nama';
    }else{
      return item.nama;
    }
  }
  _computingAlamat(item){
    if (!item) {
      return '';
    }
    if(!item.alamat){
      return 'tdk ada alamat';
    }else{
      return item.alamat;
    }
  }

  async focused (){
    super.focused();
    let response = await $.get('http://localhost/vendor-activity/www/index.php/vendor.json');
    // for (var response in obj) {
    //   console.log(obj);
    // }
    this.set('data', response.entries);
    console.log(response);

  }

  get template () {
    return html;
  }

  doLogin (evt) {
    evt.preventDefault();
    console.log(this.username);
    console.log(this.password);

    // this.__app.navigate('/');
  }
  doClick (evt) {
    event.preventDefault()
    this.set('username' ,'dio alan');

  }
}
define('dio-login', MyLogin);
